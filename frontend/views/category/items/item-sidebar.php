<?php
    use yii\helpers\Html;
?>

<div class="product">
    <div class="product-micro">
        <div class="row product-micro-row">
            <div class="col col-xs-5">
                <div class="product-image">
                    <div class="image">
                        <?= Html::a(Html::img($product->imageResize(169, 169), ['alt' => $product->name]), ['product/view', 'slug' => $product->slug]) ?>
                    </div>
                </div>
                <!-- /.product-image -->
            </div>
            <!-- /.col -->
            <div class="col col-xs-7">
                <div class="product-info">
                    <h3 class="name">
                        <?= Html::a($product->name, ['product/view', 'slug' => $product->slug]) ?>
                    </h3>
                    <div class="rating rateit-small"></div>
                    <div class="product-price"><span class="price"> $<?= $product->price ?> </span></div>
                    <!-- /.product-price -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.product-micro-row -->
    </div>
    <!-- /.product-micro -->
</div>