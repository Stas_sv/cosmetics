<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 11.11.16
 * Time: 21:36
 */


namespace frontend\assets;

use yii\web\AssetBundle;

class FontsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'//fonts.googleapis.com/css?family=Montserrat:400,700',
    '//fonts.googleapis.com/cssd767.css?family=Roboto:300,400,500,700',
    '//fonts.googleapis.com/csse262.css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800',
    '//fonts.googleapis.com/csse3e5.css?family=Montserrat:400,700',
    ];
    public $cssOptions = [
        'type' => 'text/css',
    ];
}