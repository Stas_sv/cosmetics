-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 12 2016 г., 11:35
-- Версия сервера: 10.1.19-MariaDB
-- Версия PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `keywords`, `description`) VALUES
(1, 0, 'Sportswear', NULL, NULL),
(2, 0, 'Mens', NULL, NULL),
(3, 0, 'Womens', NULL, NULL),
(4, 1, 'Nike', NULL, NULL),
(5, 1, 'Under Armour', NULL, NULL),
(6, 1, 'Adidas', NULL, NULL),
(7, 1, 'Puma', NULL, NULL),
(8, 1, 'ASICS', NULL, NULL),
(9, 2, 'Fendi', NULL, NULL),
(10, 2, 'Guess', NULL, NULL),
(11, 2, 'Valentino', NULL, NULL),
(12, 2, 'Dior', NULL, NULL),
(13, 2, 'Versace', NULL, NULL),
(14, 2, 'Armani', NULL, NULL),
(15, 2, 'Prada', NULL, NULL),
(16, 2, 'Dolce and Gabbana', NULL, NULL),
(17, 2, 'Chanel', NULL, NULL),
(18, 2, 'Gucci', NULL, NULL),
(19, 3, 'Fendi', NULL, NULL),
(20, 3, 'Guess', NULL, NULL),
(21, 3, 'Valentino', NULL, NULL),
(22, 3, 'Dior', NULL, NULL),
(23, 3, 'Versace', NULL, NULL),
(24, 0, 'Kids', NULL, NULL),
(25, 0, 'Fashion', NULL, NULL),
(26, 0, 'Households', NULL, NULL),
(27, 0, 'Interiors', NULL, NULL),
(28, 0, 'Clothing', NULL, NULL),
(29, 0, 'Bags', 'сумки ключевики...', 'сумки описание...'),
(30, 0, 'Shoes', NULL, NULL),
(31, 4, 'Sportswear', '', ''),
(32, 0, 'Mens', NULL, NULL),
(33, 0, 'Womens', NULL, NULL),
(34, 1, 'Nike', NULL, NULL),
(35, 1, 'Under Armour', NULL, NULL),
(36, 1, 'Adidas', NULL, NULL),
(37, 1, 'Puma', NULL, NULL),
(38, 1, 'ASICS', NULL, NULL),
(39, 2, 'Fendi', NULL, NULL),
(40, 2, 'Guess', NULL, NULL),
(41, 2, 'Valentino', NULL, NULL),
(42, 2, 'Dior', NULL, NULL),
(43, 2, 'Versace', NULL, NULL),
(44, 2, 'Armani', NULL, NULL),
(45, 2, 'Prada', NULL, NULL),
(46, 2, 'Dolce and Gabbana', NULL, NULL),
(47, 2, 'Chanel', NULL, NULL),
(48, 2, 'Gucci', NULL, NULL),
(49, 3, 'Fendi', NULL, NULL),
(50, 3, 'Guess', NULL, NULL),
(51, 3, 'Valentino', NULL, NULL),
(52, 3, 'Dior', NULL, NULL),
(53, 3, 'Versace', NULL, NULL),
(54, 0, 'Kids', NULL, NULL),
(55, 0, 'Fashion', NULL, NULL),
(56, 0, 'Households', NULL, NULL),
(57, 0, 'Interiors', NULL, NULL),
(58, 0, 'Clothing', NULL, NULL),
(59, 0, 'Bags', 'сумки ключевики...', 'сумки описание...'),
(60, 0, 'Shoes', NULL, NULL),
(61, 4, 'ADidasler', NULL, NULL),
(62, 9, 'ADidasler', '', ''),
(63, 32, 'Тестовая Категория1', '', ''),
(64, 0, '123', '', ''),
(65, 0, ' Проба', '123', '321'),
(66, 0, '3', '', ''),
(67, 0, '2', '', ''),
(68, 0, '123132', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `filePath` varchar(400) NOT NULL,
  `itemId` int(20) NOT NULL,
  `isMain` int(1) DEFAULT NULL,
  `modelName` varchar(150) NOT NULL,
  `urlAlias` varchar(400) NOT NULL,
  `name` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `image`
--

INSERT INTO `image` (`id`, `filePath`, `itemId`, `isMain`, `modelName`, `urlAlias`, `name`) VALUES
(1, 'Products/Product1/55f953.jpg', 1, 1, 'Product', '07c9d5b98a-1', ''),
(2, 'Products/Product1/da0aa2.jpg', 1, NULL, 'Product', 'b82b9e0a92-2', ''),
(3, 'Products/Product1/e2a3dd.jpg', 1, NULL, 'Product', 'fd91e00b6f-3', ''),
(4, 'Products/Product1/61c173.jpg', 1, NULL, 'Product', '8f129213aa-4', ''),
(5, 'Products/Product1/00fbe8.jpg', 1, NULL, 'Product', '15a0a1eb52-5', ''),
(6, 'Products/Product2/14a672.jpg', 2, 1, 'Product', 'd21cac6933-1', ''),
(7, 'Products/Product2/e7f13d.jpg', 2, NULL, 'Product', '72cc669a08-2', ''),
(8, 'Products/Product2/ba8348.jpg', 2, NULL, 'Product', 'cae6ec26af-3', ''),
(9, 'Products/Product2/e180ce.jpg', 2, NULL, 'Product', '4eb89f453a-4', ''),
(10, 'Products/Product2/6421b8.jpg', 2, NULL, 'Product', '8977178a3c-5', ''),
(11, 'Products/Product3/35851f.jpg', 3, 1, 'Product', '77ed2c824b-1', ''),
(12, 'Products/Product3/0ad00f.jpg', 3, NULL, 'Product', 'e328621664-2', ''),
(13, 'Products/Product3/37eaa8.jpg', 3, NULL, 'Product', '673eaee5d7-3', ''),
(14, 'Products/Product3/e81f8e.jpg', 3, NULL, 'Product', 'ab283d27a7-4', ''),
(15, 'Products/Product3/fc782b.jpg', 3, NULL, 'Product', 'b829db58da-5', ''),
(16, 'Products/Product4/de617f.jpg', 4, 1, 'Product', 'c34ca0c4be-1', ''),
(17, 'Products/Product4/59f6a9.jpg', 4, NULL, 'Product', '8f67cc0460-2', ''),
(18, 'Products/Product4/68f224.jpg', 4, NULL, 'Product', '13ce595f25-3', ''),
(19, 'Products/Product4/b9c71c.jpg', 4, NULL, 'Product', '79646c8652-4', ''),
(20, 'Products/Product4/d3a215.jpg', 4, NULL, 'Product', 'ef7ede1d15-5', ''),
(21, 'Products/Product5/b72ad4.jpg', 5, 1, 'Product', 'e64feca6f3-1', ''),
(22, 'Products/Product5/dfdd7f.jpg', 5, NULL, 'Product', 'c253d8f1f8-2', ''),
(23, 'Products/Product5/d550da.jpg', 5, NULL, 'Product', '23654ef218-3', ''),
(24, 'Products/Product5/895fe9.jpg', 5, NULL, 'Product', '8c3cf7819b-4', ''),
(25, 'Products/Product5/482e5e.jpg', 5, NULL, 'Product', '9d8c59bae3-5', ''),
(26, 'Products/Product6/4e680f.jpg', 6, 1, 'Product', '2c11ea3694-1', ''),
(27, 'Products/Product6/40c59b.jpg', 6, NULL, 'Product', '39f3e19230-2', ''),
(28, 'Products/Product6/65c0e7.jpg', 6, NULL, 'Product', 'e6657daf46-3', ''),
(29, 'Products/Product6/b45cc3.jpg', 6, NULL, 'Product', 'f352adb8a8-4', ''),
(30, 'Products/Product6/59fe4c.jpg', 6, NULL, 'Product', 'dd3cb3ab3b-5', ''),
(31, 'Products/Product7/456ed0.jpg', 7, 1, 'Product', '99d365be10-1', ''),
(32, 'Products/Product7/4ae679.jpg', 7, NULL, 'Product', '7792b99342-2', ''),
(33, 'Products/Product7/60bd45.jpg', 7, NULL, 'Product', '32ac88a33f-3', ''),
(34, 'Products/Product7/756732.jpg', 7, NULL, 'Product', 'dba395c619-4', ''),
(35, 'Products/Product7/1c6bd8.jpg', 7, NULL, 'Product', '8b8508df14-5', ''),
(36, 'Products/Product8/33a24e.jpg', 8, 1, 'Product', 'a980393f36-1', ''),
(37, 'Products/Product8/a79538.jpg', 8, NULL, 'Product', 'f7043ef466-2', ''),
(38, 'Products/Product8/e5cb84.jpg', 8, NULL, 'Product', '6c9719bd66-3', ''),
(39, 'Products/Product8/153af8.jpg', 8, NULL, 'Product', '84f17616b4-4', ''),
(40, 'Products/Product8/b8ff22.jpg', 8, NULL, 'Product', '8718674f64-5', ''),
(41, 'Products/Product9/1427c4.jpg', 9, 1, 'Product', '1d29191c74-1', ''),
(42, 'Products/Product9/3f4034.jpg', 9, NULL, 'Product', '175fb27692-2', ''),
(43, 'Products/Product9/749e6a.jpg', 9, NULL, 'Product', '6b8169a338-3', ''),
(44, 'Products/Product9/501056.jpg', 9, NULL, 'Product', '092d535c8b-4', ''),
(45, 'Products/Product9/f1d71d.jpg', 9, NULL, 'Product', '948ef3d490-5', ''),
(46, 'Products/Product10/b86237.jpg', 10, 1, 'Product', '5e5d8d1369-1', ''),
(47, 'Products/Product10/becd53.jpg', 10, NULL, 'Product', '5253f5c26a-2', ''),
(48, 'Products/Product10/266646.jpg', 10, NULL, 'Product', '486a6363fd-3', ''),
(49, 'Products/Product10/e90dfc.jpg', 10, NULL, 'Product', 'a18331a0df-4', ''),
(50, 'Products/Product10/7e5f1e.jpg', 10, NULL, 'Product', 'a945669332-5', ''),
(51, 'Products/Product11/5cc97f.jpg', 11, 1, 'Product', '4b50673546-1', ''),
(52, 'Products/Product11/7a3d37.jpg', 11, NULL, 'Product', 'bed62b2953-2', ''),
(53, 'Products/Product11/81d6fc.jpg', 11, NULL, 'Product', 'f081e030d2-3', ''),
(54, 'Products/Product11/df58d9.jpg', 11, NULL, 'Product', '7068c342aa-4', ''),
(55, 'Products/Product11/c4fe27.jpg', 11, NULL, 'Product', '9d266532a7-5', ''),
(56, 'Products/Product12/2d8f50.jpg', 12, 1, 'Product', '9507fa86e0-1', ''),
(57, 'Products/Product12/115bcf.jpg', 12, NULL, 'Product', '28f38b42e4-2', ''),
(58, 'Products/Product12/317c1c.jpg', 12, NULL, 'Product', '880775cdc3-3', ''),
(59, 'Products/Product12/3cb13c.jpg', 12, NULL, 'Product', '19ff47b511-4', ''),
(60, 'Products/Product12/31535c.jpg', 12, NULL, 'Product', '8351a0a517-5', ''),
(61, 'Products/Product13/f2994b.jpg', 13, 1, 'Product', '73e1488e19-1', ''),
(62, 'Products/Product13/dcc1eb.jpg', 13, NULL, 'Product', '2063940992-2', ''),
(63, 'Products/Product13/14af5c.jpg', 13, NULL, 'Product', '85c6be5318-3', ''),
(64, 'Products/Product13/239694.jpg', 13, NULL, 'Product', '5bceb7c6a9-4', ''),
(65, 'Products/Product13/533f1e.jpg', 13, NULL, 'Product', 'dd9cfda194-5', ''),
(66, 'Products/Product14/69180e.jpg', 14, 1, 'Product', 'ea1c5219d2-1', ''),
(67, 'Products/Product14/9477c0.jpg', 14, NULL, 'Product', '49c9629601-2', ''),
(68, 'Products/Product14/e172be.jpg', 14, NULL, 'Product', '604a2ac60a-3', ''),
(69, 'Products/Product14/704bbb.jpg', 14, NULL, 'Product', '5388c675e1-4', ''),
(70, 'Products/Product15/b0be9c.jpg', 15, 1, 'Product', '2a963709b4-1', ''),
(71, 'Products/Product15/f2fde7.jpg', 15, NULL, 'Product', 'ec187ed22f-2', ''),
(72, 'Products/Product15/e0d2cc.jpg', 15, NULL, 'Product', '7423524c66-3', ''),
(73, 'Products/Product15/76c760.jpg', 15, NULL, 'Product', 'b38ddafa5d-4', ''),
(74, 'Products/Product15/67e55e.jpg', 15, NULL, 'Product', '8c863b2fca-5', ''),
(75, 'Products/Product16/e5c444.jpg', 16, 1, 'Product', 'e3821ba779-1', ''),
(76, 'Products/Product16/ee1d51.jpg', 16, NULL, 'Product', 'c666f5d579-2', ''),
(77, 'Products/Product16/fd93c9.jpg', 16, NULL, 'Product', 'c011660210-3', ''),
(78, 'Products/Product16/a63d69.jpg', 16, NULL, 'Product', '36a7b514ba-4', ''),
(79, 'Products/Product16/fff028.jpg', 16, NULL, 'Product', '8faaa3bcd1-5', ''),
(80, 'Products/Product17/4ab167.jpg', 17, 1, 'Product', '82eac297a7-1', ''),
(81, 'Products/Product17/7636c1.jpg', 17, NULL, 'Product', 'f1f4191e94-2', ''),
(82, 'Products/Product17/98f6ea.jpg', 17, NULL, 'Product', '026c15272e-3', ''),
(83, 'Products/Product17/f51b17.jpg', 17, NULL, 'Product', 'df11ae8901-4', ''),
(84, 'Products/Product17/edbf0a.jpg', 17, NULL, 'Product', 'ab9be67b8d-5', ''),
(85, 'Products/Product18/f1b7c1.jpg', 18, 1, 'Product', '9bd7355eb0-1', ''),
(86, 'Products/Product18/98e0b8.jpg', 18, NULL, 'Product', '6bdcf90b27-2', ''),
(87, 'Products/Product18/bfb663.jpg', 18, NULL, 'Product', '7dc2658d7b-3', ''),
(88, 'Products/Product18/3b06b7.jpg', 18, NULL, 'Product', '1c8c97b4a6-4', ''),
(89, 'Products/Product18/7bddf6.jpg', 18, NULL, 'Product', '9fd584faca-5', ''),
(90, 'Products/Product19/8803f0.jpg', 19, 1, 'Product', 'c412a200e8-1', ''),
(91, 'Products/Product19/35def3.jpg', 19, NULL, 'Product', '0c10e400d2-2', ''),
(92, 'Products/Product19/3cbde9.jpg', 19, NULL, 'Product', '88a6ee0600-3', ''),
(93, 'Products/Product19/17490d.jpg', 19, NULL, 'Product', '41dc1a687c-4', ''),
(94, 'Products/Product19/d6d5d3.jpg', 19, NULL, 'Product', '2626a3a84b-5', ''),
(95, 'Products/Product20/72f4eb.jpg', 20, 1, 'Product', 'cc85d80d2b-1', ''),
(96, 'Products/Product20/bc105f.jpg', 20, NULL, 'Product', 'c2bc6ba573-2', ''),
(97, 'Products/Product20/a49ad9.jpg', 20, NULL, 'Product', '4608d61bdc-3', ''),
(98, 'Products/Product20/eac69f.jpg', 20, NULL, 'Product', '02bd79e83e-4', ''),
(99, 'Products/Product20/294996.jpg', 20, NULL, 'Product', 'f5f8bebfe1-5', ''),
(100, 'Products/Product21/df0f1f.jpg', 21, 1, 'Product', '537499972c-1', ''),
(101, 'Products/Product21/9be8df.jpg', 21, NULL, 'Product', '20d3a3ff6e-2', ''),
(102, 'Products/Product21/8ba4d1.jpg', 21, NULL, 'Product', '892ea1ce23-3', ''),
(103, 'Products/Product21/1c1759.jpg', 21, NULL, 'Product', '197a97e12f-4', ''),
(104, 'Products/Product21/f6e6c7.jpg', 21, NULL, 'Product', 'b7bbbf6dac-5', ''),
(105, 'Products/Product22/6ba414.jpg', 22, 1, 'Product', '56554d9cd9-1', ''),
(106, 'Products/Product22/c77cfd.jpg', 22, NULL, 'Product', '8ef77bf0c5-2', ''),
(107, 'Products/Product22/fbcc61.jpg', 22, NULL, 'Product', 'b72044c8eb-3', ''),
(108, 'Products/Product22/6d1d5c.jpg', 22, NULL, 'Product', '397c71895a-4', ''),
(109, 'Products/Product22/03955c.jpg', 22, NULL, 'Product', 'e3524bc346-5', ''),
(110, 'Products/Product23/841b3f.jpg', 23, 1, 'Product', 'cb981a0a4a-1', ''),
(111, 'Products/Product23/ab6720.jpg', 23, NULL, 'Product', '3d5c174c1d-2', ''),
(112, 'Products/Product23/953cc2.jpg', 23, NULL, 'Product', 'dfa51c4e49-3', ''),
(113, 'Products/Product23/761de1.jpg', 23, NULL, 'Product', '4aa0a3574a-4', ''),
(114, 'Products/Product23/5fa781.jpg', 23, NULL, 'Product', '35787e9093-5', ''),
(115, 'Products/Product24/a0914c.jpg', 24, 1, 'Product', 'd74a3dbd74-1', ''),
(116, 'Products/Product24/f11f12.jpg', 24, NULL, 'Product', '04bec191ad-2', ''),
(117, 'Products/Product24/1f0965.jpg', 24, NULL, 'Product', '3b2831b0cc-3', ''),
(118, 'Products/Product24/47cef0.jpg', 24, NULL, 'Product', 'fb2c238fb7-4', ''),
(119, 'Products/Product24/1f5543.jpg', 24, NULL, 'Product', '70a0e4ea82-5', ''),
(120, 'Products/Product25/759ec6.jpg', 25, 1, 'Product', 'c4f9e850bd-1', ''),
(121, 'Products/Product25/b5bd44.jpg', 25, NULL, 'Product', '4bd442be4c-2', ''),
(122, 'Products/Product25/081e73.jpg', 25, NULL, 'Product', '72a9790416-3', ''),
(123, 'Products/Product25/750bd3.jpg', 25, NULL, 'Product', '08afeeaa29-4', ''),
(124, 'Products/Product25/de7410.jpg', 25, NULL, 'Product', 'a2306404fa-5', ''),
(125, 'Products/Product26/56ce36.jpg', 26, 1, 'Product', 'a75dd2ccc3-1', ''),
(126, 'Products/Product26/156820.jpg', 26, NULL, 'Product', '933dc13e21-2', ''),
(127, 'Products/Product26/2d9f91.jpg', 26, NULL, 'Product', 'c4e5d705a2-3', ''),
(128, 'Products/Product26/c2e9a6.jpg', 26, NULL, 'Product', 'f48c3c96ba-4', ''),
(129, 'Products/Product26/2422b1.jpg', 26, NULL, 'Product', '5e1072c911-5', ''),
(130, 'Products/Product27/7e7b61.jpg', 27, 1, 'Product', '9ca8bb8a79-1', ''),
(131, 'Products/Product27/72329f.jpg', 27, NULL, 'Product', '3b59c298de-2', ''),
(132, 'Products/Product27/2ba550.jpg', 27, NULL, 'Product', '7bfbf77395-3', ''),
(133, 'Products/Product27/b1370b.jpg', 27, NULL, 'Product', '67945d768c-4', ''),
(134, 'Products/Product27/79fb0c.jpg', 27, NULL, 'Product', '3358716eaa-5', ''),
(135, 'Products/Product28/8fa935.jpg', 28, 1, 'Product', '634c8e0e33-1', ''),
(136, 'Products/Product28/a24531.jpg', 28, NULL, 'Product', '3612e8b2d8-2', ''),
(137, 'Products/Product28/29fee4.jpg', 28, NULL, 'Product', 'd38d2b1f76-3', ''),
(138, 'Products/Product28/968bed.jpg', 28, NULL, 'Product', '0ddd10e159-4', ''),
(139, 'Products/Product28/20b47c.jpg', 28, NULL, 'Product', '90638b780d-5', ''),
(140, 'Products/Product29/889627.jpg', 29, 1, 'Product', 'e795bd476b-1', ''),
(141, 'Products/Product30/9c0c3e.jpg', 30, 1, 'Product', 'a5d344075f-1', ''),
(142, 'Products/Product30/940ae3.jpg', 30, NULL, 'Product', '825d7faf15-2', ''),
(143, 'Products/Product30/b09462.jpg', 30, NULL, 'Product', '0d3c2e4c1e-3', ''),
(144, 'Products/Product30/6f55b5.jpg', 30, NULL, 'Product', '427899f6d2-4', ''),
(145, 'Products/Product30/f07602.jpg', 30, NULL, 'Product', '33669e0761-5', ''),
(146, 'Products/Product31/234e4f.jpg', 31, 1, 'Product', '71dde003d8-1', ''),
(147, 'Products/Product31/1129bc.jpg', 31, NULL, 'Product', '33472ab078-2', ''),
(148, 'Products/Product31/10b82b.jpg', 31, NULL, 'Product', '38839a1561-3', ''),
(149, 'Products/Product31/489f5c.jpg', 31, NULL, 'Product', '33a6f348b3-4', ''),
(150, 'Products/Product31/9f8e2f.jpg', 31, NULL, 'Product', '7c546e1236-5', ''),
(151, 'Products/Product32/7200fc.jpg', 32, 1, 'Product', '630522929a-1', ''),
(152, 'Products/Product32/925342.jpg', 32, NULL, 'Product', '6b70311921-2', ''),
(153, 'Products/Product32/532edf.jpg', 32, NULL, 'Product', '375efcbcd4-3', ''),
(154, 'Products/Product33/ca68a7.jpg', 33, 1, 'Product', '7cec099f9e-1', ''),
(155, 'Products/Product33/e6c648.jpg', 33, NULL, 'Product', 'bbdcdf4a83-2', ''),
(156, 'Products/Product33/719786.jpg', 33, NULL, 'Product', 'd969bddabc-3', ''),
(157, 'Products/Product34/9a084f.jpg', 34, 1, 'Product', '464e937a09-1', ''),
(158, 'Products/Product34/001609.jpg', 34, NULL, 'Product', '89b29819af-2', ''),
(159, 'Products/Product34/2614f3.jpg', 34, NULL, 'Product', '14c71217b3-3', ''),
(160, 'Products/Product34/5781e8.jpg', 34, NULL, 'Product', 'ff022e369b-4', ''),
(161, 'Products/Product34/1ee739.jpg', 34, NULL, 'Product', '79ca7e33e5-5', ''),
(162, 'Products/Product35/4935fc.jpg', 35, 1, 'Product', 'c3e77fd293-1', ''),
(163, 'Products/Product35/6d0cfb.jpg', 35, NULL, 'Product', '01a592f449-2', '');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1481478445),
('m130524_201442_init', 1481478451);

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `qty` int(10) NOT NULL,
  `sum` float NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `created_at`, `updated_at`, `qty`, `sum`, `status`, `name`, `email`, `phone`, `address`) VALUES
(1, '2016-12-12 12:32:47', '2016-12-12 12:32:47', 1001, 20, '0', 'Stas', 'stassimonov@gmail.com', '0000-000-123', 'Geroev Truda str. 31-B app. 29');

-- --------------------------------------------------------

--
-- Структура таблицы `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `qty_item` int(11) NOT NULL,
  `sum_item` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `name`, `price`, `qty_item`, `sum_item`) VALUES
(1, 1, 3, 'Блуза Mango 53005681-05 M Бежевая', 20, 1, 20),
(2, 1, 7, 'Кардиган ONLY ON 15102048 M Black Tan/Partridg', 0, 1000, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text,
  `price` float NOT NULL DEFAULT '0',
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT 'no-image.png',
  `hit` enum('0','1') NOT NULL DEFAULT '0',
  `new` enum('0','1') NOT NULL DEFAULT '0',
  `sale` enum('0','1') NOT NULL DEFAULT '0',
  `recomend` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `category_id`, `name`, `content`, `price`, `keywords`, `description`, `img`, `hit`, `new`, `sale`, `recomend`) VALUES
(1, 4, 'Джинсы Garcia 244/32/856 28-32 р Серо-синие', '<p>Великолепные джинсы винтажно-голубого цвета. Настоящая находка для любителей качественного денима. Особенности: Зауженные к низу Пять карманов Высококачественный деним Высокая посадка (high fit) Выгодно подчеркивают фигуру</p>\r\n', 10, '', '', 'product1.jpg', '0', '0', '0', '0'),
(2, 4, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', '<p>MR520 &ndash; амбициозный восточноевропейский бренд, который предлагает качественную и стильную одежду, сделанную специально для молодых людей, следящих за своим внешним видом. Женские джинсы фасона boyfriend fit (в переводе с англ. &ndash; &quot;джинсы моего парня&quot;). Модель с зауженными штанинами. Застегивается на пуговицы. Изделие с низкой посадкой. Джинсы дополнены прорезными карманами спереди и накладными карманами сзади. Изделие декорировано эффектом потертости, вареным эффектом и необычными швами.</p>\r\n', 56, '', '', 'product2.jpg', '1', '0', '0', '0'),
(3, 9, 'Блуза Mango 53005681-05 M Бежевая', '<p>Испанский бренд модной одежды &quot;Mango&quot; родился в 1984 году в Барселоне, где и по сей день находится штаб-квартира компании. В том же городе появился и первый магазин &mdash; на улице Пасео де Грасия (Paseo de Gracia). Компания, основанная братьями Исааком и Нахманом Андиком (Isaac &amp; Nahman Andic), очень быстро набрала популярность &mdash; всего лишь годом позднее был открыт магазин в другом городе, на этот раз в Валенсии. Одежда &quot;Mango&quot; отличается высоким качеством, приемлемой ценой, современным дизайном и неповторимым стилем.</p>\r\n', 20, '', '', 'product3.jpg', '1', '1', '0', '0'),
(4, 21, 'Блуза Tom Tailor TT 20312490071 7610 M Зелёная', '<p>Tom Tailor Group &mdash; один из ведущих и быстро развивающихся Fashion холдингов германии и европы, продукция которого ориентирована на целевую аудиторию в возрасте от 0 до 60 лет. Tom Tailor известен на рынке текстиля с 1962 года и до сих пор сохраняет стандарты немецкого качества. Tom Tailor предлагает повседневную одежду и аксессуары высокого качества для женщин, мужчин и детей. Одежда от Tom Tailor поможет создать активный повседневный образ с нотками элегантности.</p>\r\n', 70, '', '', 'product4.jpg', '1', '0', '1', '0'),
(5, 25, 'Блузка Kira Plastinina 17-16-17453-SM-29 S', '', 0, '', '', 'product5.jpg', '1', '0', '0', '0'),
(6, 28, 'Кардиган Levi''s Icy Grey Heather M', '', 100, '', '', 'product6.jpg', '1', '0', '0', '0'),
(7, 28, 'Кардиган ONLY ON 15102048 M Black Tan/Partridg', '<p>Casual марка молодежной женской одежды ONLY является частью датской компании Bestseller AS. Изначально Bestseller занимался производством детской одежды, а в 1995 году появилась на свет марка ONLY. Популярность этой марки возрастала быстрыми темпами и теперь ONLY владеет 770 магазинами в более чем 40 странах мира. ONLY &mdash; бренд стильной молодежной одежды. Это бренд для тех, кто любит добиваться успеха и быть не таким, как все. Демократичные цены, модные модели, экологически чистые ткани делают одежду от ONLY сверхпопулярной.</p>\r\n', 0, '', '', 'no-image.png', '1', '1', '0', '0'),
(8, 26, 'Брюки SK House 2211-1972кор L Коричневые', '<p>Компания SK House &mdash; это украинский производитель модной женской одежды с безупречной репутацией и тысячами поклонников по всему СНГ. SK House изготавливает качественный и долговечный товар, созданный из высококачественных тканей. Компания использует современные методы пошива и, изучая текущие мировые тенденции и локальные требования покупателей, создает модели, которые не задерживаются на полках длительное время и быстро раскупаются во всех странах.</p>\r\n', 99, '', '', 'no-image.png', '0', '0', '1', '0'),
(9, 26, 'Брюки Kira Plastinina 17-07-17418-FL-58 L', '', 0, '', '', 'product1.jpg', '0', '0', '0', '0'),
(10, 29, 'Сумка GUSSACI TUGUS13A060-3-10', '<p>Простота, инновационный стиль бренда, высококачественные требования к продукции, благодаря этому GUSSACI Italy пользуется высокой репутацией во многих странах Европы. Превосходное качество, отличный дизайн, соответствующие цены делают &quot;Гуссачи&quot; модным и популярным! Особенности: Количество основных отделений: 1. Сумка имеет прорезной карман на молнии, а также два небольших накладных кармана для хранения мобильного телефона, разных портативных гаджетов и мелочей. На лицевой стороне модели есть узкий прорезной карман на &quot;молнии&quot;. На тыльной стороне модели есть прорезной карман на &quot;молнии&quot;. Особенностью данной модели является возможность изменения ее формы при помощи дополнительной внешней застежки-молнии. Сумка имеет 2 ручки для переноса на локте или в руке. Их длина не регулируется и составляет 45 см, а высота от крайней точки ручки до сумки &mdash; 16 см. В комплект к изделию прилагается съемный плечевой ремень. Его длина может регулироваться при помощи металлической пряжки от 78 до 137.5 см. Сумка закрывается при помощи застежки-молнии. Материал подкладки: плотная ткань. Материал сумки: кожезаменитель. Цвет фурнитуры: золото. Размеры сумки: 26 х 25 х 10.5 см</p>\r\n', 15, '', '', 'product3.jpg', '0', '1', '0', '0'),
(11, 29, 'Cумка Michael Kors Jet Set Travel Нежно-розовая', '<p>Особенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом. Модели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.</p>\r\n', 200, '', '', 'no-image.png', '0', '0', '1', '0'),
(12, 29, 'Cумка Michael Kors Selma Золотистая', '<p>Особенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом. Модели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.</p>\r\n', 205, '', '', 'product5.jpg', '0', '0', '0', '0'),
(13, 29, 'Cумка Michael Kors Bedford Красная', '<p>Особенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом. Модели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.</p>\r\n', 0, '', '', 'no-image.png', '0', '0', '0', '0'),
(14, 29, 'Cумка Michael Kors JS Travel Светло-розовая', '<p>Особенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом. Модели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.</p>\r\n', 0, '', '', 'no-image.png', '0', '0', '0', '0'),
(15, 2, 'Джинсы Garcia 244/32/856 28-32 р Серо-синие', '<p><img alt="" src="/upload/global/some/sub/path/WVoEcTr.png" style="border-style:solid; border-width:10px; float:right; height:300px; margin:20px 10px; width:169px" />Великолепные <em><strong>джинсы</strong></em> <s>винт</s>ажно-голубого цвета. Настоящая находка для любителей качественного денима. Особенности: Зауженные к низу Пять карманов Высококачественный деним Высокая посадка (high fit) Выгодно подчеркивают фигуру</p>\r\n', 10, '', '', 'p1.jpg', '0', '1', '0', '0'),
(16, 2, 'Джинсы MR520 MR 227 20002 0115 29-34 р Синие', '<p>MR520 &ndash; амбициозный восточноевропейский бренд, который предлагает качественную и стильную одежду, сделанную специально для молодых людей, следящих за своим внешним видом. Женские джинсы фасона boyfriend fit (в переводе с англ. &ndash; &quot;джинсы моего парня&quot;). Модель с зауженными штанинами. Застегивается на пуговицы. Изделие с низкой посадкой. Джинсы дополнены прорезными карманами спереди и накладными карманами сзади. Изделие декорировано эффектом потертости, вареным эффектом и необычными швами.</p>\r\n', 56, '', '', 'p2.jpg', '1', '0', '0', '0'),
(17, 9, 'Блуза Mango 53005681-05 M Бежевая', '<p>Испанский бренд модной одежды &quot;Mango&quot; родился в 1984 году в Барселоне, где и по сей день находится штаб-квартира компании. В том же городе появился и первый магазин &mdash; на улице Пасео де Грасия (Paseo de Gracia). Компания, основанная братьями Исааком и Нахманом Андиком (Isaac &amp; Nahman Andic), очень быстро набрала популярность &mdash; всего лишь годом позднее был открыт магазин в другом городе, на этот раз в Валенсии. Одежда &quot;Mango&quot; отличается высоким качеством, приемлемой ценой, современным дизайном и неповторимым стилем.</p>\r\n', 20, '', '', 'p3.jpg', '0', '0', '1', '0'),
(18, 21, 'Блуза Tom Tailor TT 20312490071 7610 M Зелёная', '<p>Tom Tailor Group &mdash; один из ведущих и быстро развивающихся Fashion холдингов германии и европы, продукция которого ориентирована на целевую аудиторию в возрасте от 0 до 60 лет. Tom Tailor известен на рынке текстиля с 1962 года и до сих пор сохраняет стандарты немецкого качества. Tom Tailor предлагает повседневную одежду и аксессуары высокого качества для женщин, мужчин и детей. Одежда от Tom Tailor поможет создать активный повседневный образ с нотками элегантности.</p>\r\n', 70, '', '', 'p4.jpg', '0', '1', '0', '0'),
(19, 25, 'Блузка Kira Plastinina 17-16-17453-SM-29 S', '', 0, '', '', 'p5.jpg', '0', '0', '1', '0'),
(20, 28, 'Кардиган Levi''s Icy Grey Heather M', '', 100, 'Кардиган супер', 'Экстра', 'p6.jpg', '1', '0', '0', '0'),
(21, 29, 'Кардиган ONLY ON 15102048 M Black Tan/Partridg', '<p>Casual марка молодежной женской одежды ONLY является частью датской компании Bestseller AS. Изначально Bestseller занимался производством детской одежды, а в 1995 году появилась на свет марка ONLY. Популярность этой марки возрастала быстрыми темпами и теперь ONLY владеет 770 магазинами в более чем 40 странах мира. ONLY &mdash; бренд стильной молодежной одежды. Это бренд для тех, кто любит добиваться успеха и быть не таким, как все. Демократичные цены, модные модели, экологически чистые ткани делают одежду от ONLY сверхпопулярной.</p>\r\n', 0, '', '', 'p7.jpg', '0', '0', '1', '0'),
(22, 29, 'Брюки SK House 2211-1972кор L Коричневые', '<p>Компания SK House &mdash; это украинский производитель модной женской одежды с безупречной репутацией и тысячами поклонников по всему СНГ. SK House изготавливает качественный и долговечный товар, созданный из высококачественных тканей. Компания использует современные методы пошива и, изучая текущие мировые тенденции и локальные требования покупателей, создает модели, которые не задерживаются на полках длительное время и быстро раскупаются во всех странах.</p>\r\n', 99, '', '', 'p18.jpg', '0', '1', '0', '0'),
(23, 29, 'Брюки Kira Plastinina 17-07-17418-FL-58 L', '', 0, '', '', 'p8.jpg', '1', '0', '0', '0'),
(24, 29, 'Сумка GUSSACI TUGUS13A060-3-10', '<p>Простота, инновационный стиль бренда, высококачественные требования к продукции, благодаря этому GUSSACI Italy пользуется высокой репутацией во многих странах Европы. Превосходное качество, отличный дизайн, соответствующие цены делают &quot;Гуссачи&quot; модным и популярным! Особенности: Количество основных отделений: 1. Сумка имеет прорезной карман на молнии, а также два небольших накладных кармана для хранения мобильного телефона, разных портативных гаджетов и мелочей. На лицевой стороне модели есть узкий прорезной карман на &quot;молнии&quot;. На тыльной стороне модели есть прорезной карман на &quot;молнии&quot;. Особенностью данной модели является возможность изменения ее формы при помощи дополнительной внешней застежки-молнии. Сумка имеет 2 ручки для переноса на локте или в руке. Их длина не регулируется и составляет 45 см, а высота от крайней точки ручки до сумки &mdash; 16 см. В комплект к изделию прилагается съемный плечевой ремень. Его длина может регулироваться при помощи металлической пряжки от 78 до 137.5 см. Сумка закрывается при помощи застежки-молнии. Материал подкладки: плотная ткань. Материал сумки: кожезаменитель. Цвет фурнитуры: золото. Размеры сумки: 26 х 25 х 10.5 см</p>\r\n', 15, '', '', 'p9.jpg', '0', '1', '0', '0'),
(25, 29, 'Cумка Michael Kors Jet Set Travel Нежно-розовая', '<p>Особенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом. Модели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.</p>\r\n', 200, '', '', 'p10.jpg', '0', '0', '1', '0'),
(26, 29, 'Cумка Michael Kors Selma Золотистая', '<p>Особенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом. Модели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.</p>\r\n', 205, '', '', 'p11.jpg', '0', '0', '0', '0'),
(27, 29, 'Cумка Michael Kors Bedford Красная', '<p>Особенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом. Модели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.</p>\r\n', 0, '', '', 'p12.jpg', '1', '0', '0', '0'),
(28, 29, 'Cумка Michael Kors JS Travel Светло-розовая', '<p>Особенность стиля Michael Kors заключается в том, что простота его коллекций гармонирует с роскошью. Этому дизайнеру под силу объединить американский утилитаризм в манере одеваться с европейской изысканностью и шармом. Все его работы отличает изящная утонченность, которая рождается из строгих, почти примитивных линий. Все аксессуары поддерживают общий стиль человека с безупречным вкусом. Модели Michael Kors могут оставаться оригинальными, стильными и неотразимыми в течение многих лет, что особо важно для покупательниц, не желающих постоянно обновлять свой гардероб.</p>\r\n', 0, '', '', 'p13.jpg', '0', '0', '1', '0'),
(29, 11, 'Товар новый 1', '<p>Описание товара 1</p>\r\n', 1000000, '', '', 'p4.jpg', '1', '0', '0', '0'),
(30, 11, 'Товар новый 2', '<p>Описание товара 2</p>\r\n', 132, '', '', 'p3.jpg', '1', '0', '0', '0'),
(31, 11, 'Товар новый 3', '<p>Описание товара 3</p>\r\n', 321, '', '', 'p30.jpg', '0', '0', '1', '0'),
(32, 12, 'Товар новый 4', '<p>Описание товара 4</p>\r\n', 78, '', '', 'p29.jpg', '0', '1', '0', '1'),
(33, 12, 'Товар новый 5', '<p>Описание товара 5</p>\r\n', 55, '', '', 'p1.jpg', '0', '0', '1', '1'),
(34, 11, 'Рекомендованный 1', '<p>Описание рекомендованного товара 1</p>\r\n', 1000000, '', '', 'p4.jpg', '0', '1', '0', '1'),
(35, 15, 'Ожиотажные товары 1', '<p>Описание ожиотажных товаров</p>\r\n', 112, '', '', 'p20.jpg', '1', '0', '0', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'stas123', 'CT_YVBmP09vSqA_FcW47mbo64s6sBZOX', '$2y$13$bpxDTZhJ4yp5PVk4zbMOVuxGIbbwcSkdlicJ8ErALUnewPeMmLqBC', NULL, 'stassimonov@gmail.com', 10, 1481479118, 1481479118);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT для таблицы `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;
--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
