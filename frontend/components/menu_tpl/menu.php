<li class="dropdown menu-item">

    <a href="<?= \yii\helpers\Url::to(['category/view','id'=>$category['slug']]) ?>" >
        <?php if( isset($category['childs']) ): ?>
        <i class="icon fa fa-shopping-bag" aria-hidden="true"></i>
        <?php endif;?>
        <?= $category['name']?>
    </a>


    <?php if( isset($category['childs']) ): ?>

        <ul class="nav" style="padding-left: 10px;">
            <?= $this->getMenuHtml($category['childs'])?>
        </ul>
    <?php endif;?>


</li>