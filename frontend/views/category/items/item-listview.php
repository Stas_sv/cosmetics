<?php
use yii\helpers\Html;
?>

<div class="category-product-inner wow fadeInUp">
    <div class="products">
        <div class="product-list product">
            <div class="row product-list-row">
                <div class="col col-sm-4 col-lg-4">
                    <div class="product-image">
                        <div class="image">  <?= Html::a(Html::img($product->imageResize(189, 189), ['alt' => $product->name]), ['product/view', 'slug'=>$product->slug]) ?> </div>
                    </div>
                    <!-- /.product-image -->
                </div>
                <!-- /.col -->
                <div class="col col-sm-8 col-lg-8">
                    <div class="product-info">
                        <h3 class="name"><?= Html::a($product->name, ['product/view', 'slug'=>$product->slug])?></h3>
                        <div class="rating rateit-small"></div>
                        <div class="product-price"> <span class="price"> $<?= $product->price?> </span> <span class="price-before-discount">$ 800</span> </div>
                        <!-- /.product-price -->
                        <div class="description m-t-10"><?= $product->content ?></div>
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">

                                    <li class="lnk wishlist"> <a class="add-to-cart" href="detail.html" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                                    <li class="lnk"> <a class="add-to-cart" href="detail.html" title="Compare"> <i class="fa fa-signal"></i> </a> </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->

                    </div>
                    <!-- /.product-info -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.product-list-row -->

            <?php if($product->new): ?>
                <div class="tag new"><span><?php echo Yii::t('frontend/category/product', 'new')?></span></div>
            <?php endif?>

            <?php if($product->sale): ?>
                <div class="tag sale"><span><?php echo Yii::t('frontend/category/product', 'sale')?></span></div>
            <?php endif?>

            <?php if($product->sale):?>
                <div class="tag hot"><span><?php echo Yii::t('frontend/category/product', 'hot')?></span></div>
            <?php endif?>
        </div>
        <!-- /.product-list -->
    </div>
    <!-- /.products -->
</div>