<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        //'css/site.css',
        //'assets/css/bootstrap.min.css',
        'assets/css/main.css',
        'assets/css/blue.css',
        'assets/css/owl.carousel.css',
        'assets/css/owl.transitions.css',
        'assets/css/animate.min.css',
        'assets/css/rateit.css',
        //'assets/css/bootstrap-select.min.css',
        'assets/css/font-awesome.css',
    ];
    public $js = [
        'assets/js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
