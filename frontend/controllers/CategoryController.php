<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 15.11.16
 * Time: 18:10
 */

namespace frontend\controllers;
use frontend\models\Category;
use frontend\models\Product;
use Yii;
//use yii\data\Pagination;
use yii\data\ActiveDataProvider;


class CategoryController extends AppController
{
public function actionIndex()
{
    $hits= Product::find()->where(['hit'=>'1'])->limit(6)->all();
    $recommended = Product::find()->where(['recomend'=>'1'])->limit(9)->all();
    $new = Product::find()->where(['new'=>'1'])->limit(10)->all();
    $this->setMeta('Каталог Косметический');

    //debug($recommended);
    return $this->render('index', compact('hits', 'recommended', 'new'));
}

 public function actionView($id)
 {
     $id=Yii::$app->request->get('id');
     $category =Category::find()->where(['slug'=>$id])->one();
     if (empty($category))
     {
         throw new \yii\web\HttpException(404, 'Такой категории нет.');
     }
     //$products = Product::find()->where(['category_id'=>$id])->all();
     //$query = Product::find()->where(['category_id'=>$id]);
     //$pages = new Pagination(['totalCount'=>$query->count(),'pageSize'=>3, 'forcePageParam'=>false,'pageSizeParam'=>false ]);
     //$products = $query->offset($pages->offset)->limit($pages->limit)->all();
     $dataProvider= new ActiveDataProvider(['query'=>Product::find()->where(['category_id'=>$category['id']]),'pagination'=>['pageSize'=>6,'forcePageParam'=>false,'pageSizeParam'=>false]]);

     $this->setMeta('Каталог Косметический  | ' . $category->name, $category->keywords, $category->description);

     return $this->render('view',compact('dataProvider','category'));

 }


public function actionSearch()
{
    $q= trim(Yii::$app->request->get('q'));
    $this->setMeta('Поиск по каталогу  | ' . $q);
    if (!$q)
    {
        return $this->render('search');
    }
    $dataProvider= new ActiveDataProvider(['query'=>Product::find()->where(['like','name',$q]),'pagination'=>['pageSize'=>6,'forcePageParam'=>false,'pageSizeParam'=>false]]);


    return $this->render('search',compact('dataProvider','q'));
}

}