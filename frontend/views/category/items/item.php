<?php
    use yii\helpers\Html;
?>

<div class="products">
    <div class="product">
        <div class="product-image">
            <div class="image">
                <?= Html::a(Html::img($product->imageResize(189, 189), ['alt' => $product->name]), ['product/view', 'slug'=>$product->slug]) ?>
            </div>

            <?php if($product->new): ?>
                <div class="tag new"><span><?php echo Yii::t('frontend/category/product', 'new')?></span></div>
            <?php endif?>

            <?php if($product->sale): ?>
                <div class="tag sale"><span><?php echo Yii::t('frontend/category/product', 'sale')?></span></div>
            <?php endif?>

            <?php if($product->sale):?>
                <div class="tag hot"><span><?php echo Yii::t('frontend/category/product', 'hot')?></span></div>
            <?php endif?>
        </div>
        <!-- /.product-image -->
        <div class="product-info text-left">
            <h3 class="name"><?= Html::a($product->name, ['product/view', 'slug'=>$product->slug])?></h3>
            <div class="rating rateit-small"></div>
            <div class="description"></div>
            <div class="product-price">
                <span class="price"> $<?= $product->price?> </span>
                <span class="price-before-discount">$ 800</span>
            </div>
            <!-- /.product-price -->
        </div>
    </div>
    <!-- /.product -->
</div>