<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 15.11.16
 * Time: 18:11
 */

namespace frontend\controllers;


use yii\web\Controller;

class AppController extends Controller
{
 protected function setMeta($title=null, $keywords=null, $description=null)
 {
      $this->view->title=$title;
      $this->view->registerMetaTag(['name'=>'keywords', 'content'=>"$keywords"]);
      $this->view->registerMetaTag(['name'=>'description', 'content'=>"$description"]);
 }
}