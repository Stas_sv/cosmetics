<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@frontend-web', dirname(dirname(__DIR__)) . '/frontend/web');

Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@backend-web', dirname(dirname(__DIR__)) . '/backend/web');

Yii::setAlias('@upload', dirname(dirname(__DIR__)) . '/frontend/web/upload');
Yii::setAlias('@upload-cache', dirname(dirname(__DIR__)) . '/frontend/web/upload/cache');
Yii::setAlias('@upload-product', dirname(dirname(__DIR__)) . '/frontend/web/upload/products');
