<?php

use yii\db\Migration;
use yii\db\Schema;

class m161227_155456_123 extends Migration
{
    public function up()
    {
        $this->createTable('{{%category_product}}', [
            'id' => Schema::TYPE_PK,
            'id_category' => Schema::TYPE_INTEGER . "(11) NOT NULL",
            'id_product' => Schema::TYPE_INTEGER . "(11) NOT NULL",
        ] );
    }

    

    public function down()
    {
        echo "m161227_155456_123 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
