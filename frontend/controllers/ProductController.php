<?php

namespace frontend\controllers;
use frontend\models\Category;
use frontend\models\Product;
use Yii;

class ProductController extends AppController
{
    public function actionView($id)
    {
        $id= Yii::$app->request->get('id');
        $product =Product::find()->where(['slug'=>$id])->one();
        $category =Category::find()->where(['id'=>$product->category_id])->one();
        if (empty($product))
        {
            throw new \yii\web\HttpException(404, 'Такого продукта нет в каталоге.');
        }
        //$product= Product::find()->with('category')->where(['id'=>$id])->limit(1)->one();
        $this->setMeta('Каталог Косметический  | ' . $product->name, $product->keywords, $product->description);




        return $this->render('view', compact('product','category'));
    }

}
