<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
    //'assets/css/bootstrap.min.css',
    'assets/css/main.css',
    'assets/css/blue.css',
    'assets/css/owl.carousel.css',
    'assets/css/owl.transitions.css',
    'assets/css/animate.min.css',
    'assets/css/rateit.css',
    //'assets/css/bootstrap-select.min.css',
    'assets/css/font-awesome.css',
    ];
    public $js = [

//'assets/js/jquery.js',
//'assets/js/bootstrap.min.js',
//'assets/js/bootstrap-hover-dropdown.min.js',
'assets/js/owl.carousel.min.js',
'assets/js/echo.min.js',
'assets/js/jquery.easing-1.3.min.js',
//'assets/js/bootstrap-slider.min.js',
'assets/js/jquery.rateit.min.js',
'assets/js/lightbox.min.js',
//'assets/js/bootstrap-select.min.js',

'assets/js/wow.min.js',
'assets/js/scripts.js',
      //  'assets/js/jquery.scrollUp.min.js',
        'assets/js/price-range.js',
        //'assets/js/jquery.prettyPhoto.js',
        'assets/js/jquery.cookie.js',
        'assets/js/jquery.accordion.js',
        'assets/js/main.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
